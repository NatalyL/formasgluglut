# formasgluglut

Diseño de figuras con glu y glut en tres dimensiones con rotacion y traslacion.

Mi proyecto consta de una clase que es la clase principal:
- FormasGluYGlut

Las figuras realizadas son:
- Disco solido
- Esfera solida
- Esfera con lineas
- Cono solido
- Cono con lineas
- Cilindro solido
- Cilindro con lineas
- Cilindro contorno
- Cubo solido
- Cubo con lineas
- Toro/Dona solido
- Toro/ Dona con lineas 
- Dodecaedro solido
- Dodecaedro con lineas
- Isocaedro solido
- Isocaedro con lineas
- Tetera solida
- Tetera con lineas
- Octaedro solido
- Octaedro con lineas
- Tetraedro solido
- Tetraedro con lineas

Se hizo uso de gluQuadricDrawStyle, glPushMatrix, glPopMatrix, Translate y Rotate