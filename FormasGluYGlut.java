package org.nlopez;

import com.sun.opengl.util.Animator;
import com.sun.opengl.util.GLUT;
import java.awt.Frame;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCanvas;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.glu.GLU;
import javax.media.opengl.glu.GLUquadric;

/**
 * FormasGluYGlut.java <BR>
 * author: Brian Paul (converted to Java by Ron Cemer and Sven Goethel)
 * <P>
 *
 * This version is equal to Brian Paul's version 1.2 1999/10/21
 */
public class FormasGluYGlut implements GLEventListener, KeyListener {

    static float angulo = 0;
    float x = 0;

    public static void main(String[] args) {
        Frame frame = new Frame("Primitivas 3D usando GLU y GLUT");
        GLCanvas canvas = new GLCanvas();

        canvas.addGLEventListener(new FormasGluYGlut());
        frame.add(canvas);
        frame.setSize(850, 680);
        final Animator animator = new Animator(canvas);
        frame.addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {
                // Run this on another thread than the AWT event queue to
                // make sure the call to Animator.stop() completes before
                // exiting
                new Thread(new Runnable() {

                    public void run() {
                        animator.stop();
                        System.exit(0);
                    }
                }).start();
            }
        });
        // Center frame
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        animator.start();
    }

    public void init(GLAutoDrawable drawable) {
        // Use debug pipeline
        // drawable.setGL(new DebugGL(drawable.getGL()));

        GL gl = drawable.getGL();
        System.err.println("INIT GL IS: " + gl.getClass().getName());

        // Enable VSync
        gl.setSwapInterval(1);

        // Setup the drawing area and shading mode
        gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        gl.glShadeModel(GL.GL_SMOOTH); // try setting this to GL_FLAT and see what happens.
        drawable.addKeyListener(this);
    }

    public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
        GL gl = drawable.getGL();
        GLU glu = new GLU();

        if (height <= 0) { // avoid a divide by zero error!

            height = 1;
        }
        final float h = (float) width / (float) height;
        gl.glViewport(0, 0, width, height);
        gl.glMatrixMode(GL.GL_PROJECTION);
        gl.glLoadIdentity();
        glu.gluPerspective(21.0f, h, 10.0, 145.0);
        //gl.glOrtho(-15, 15, -15, 15, -100, 200);//1.valor izquierda,2.derecha,3 hacia abajo 
        gl.glMatrixMode(GL.GL_MODELVIEW);
        gl.glLoadIdentity();
    }

    public void display(GLAutoDrawable drawable) {
        GL gl = drawable.getGL();
        GLU glu = new GLU();
        GLUT glut = new GLUT();

        // Clear the drawing area
        gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);
        // Reset the current matrix to the "identity"
        gl.glLoadIdentity();
        gl.glEnable(GL.GL_DEPTH_TEST);

        // Move the "drawing cursor" around
        gl.glTranslatef(0, 0, -100);
       // glu.gluLookAt(a, a, 0, 0, 0, 0, 0, 1, 0);
        //gl.glRotatef(angulo, 1, 0, 0);

        GLUquadric quad;
        quad = glu.gluNewQuadric();
        glu.gluQuadricDrawStyle(quad, GLU.GLU_FILL);

        //Disco solido
        gl.glPushMatrix();
        gl.glColor3f(0.8f, 5, 1);
        gl.glScalef(1, 1, 2);
        gl.glTranslatef(-14f, 21f, -20f);
        gl.glRotatef(60, 0, 1, 0);
        gl.glRotatef(angulo, 1, -1, 0);
        glu.gluDisk(quad, 1f, 3, 50, 50);
        gl.glPopMatrix();

        //Esfera solida
        gl.glPushMatrix();
        gl.glColor3f(1, 0, 1);
        gl.glTranslatef(0f, 10f, -20f);
        gl.glRotatef(45, 0, 1, 0);
        gl.glRotatef(angulo, 1, 0, 0);
        glut.glutSolidSphere(2.3f, 20, 20);
        gl.glPopMatrix();

        //Esfera
        gl.glPushMatrix();
        gl.glTranslatef(0f, 1f, 3f);
        gl.glRotatef(45, 0, 1, 0);
        gl.glRotatef(angulo, 1, 0, 0);
        gl.glColor3f(0, 1, 0);
        glut.glutWireSphere(2, 12, 12);
        gl.glPopMatrix();

        //Cono
        gl.glPushMatrix();
        gl.glTranslatef(-18f, 1.5f, 0f);
        gl.glRotatef(45, 0, 1, 0);
        gl.glRotatef(angulo, 1, 0, 0);
        gl.glColor3f(0, 1, 1);
        glut.glutWireCone(1.5f, 3, 10, 8);
        gl.glPopMatrix();

        //Cono solido
        gl.glPushMatrix();
        gl.glTranslatef(-18f, 8.8f, 0f);
        gl.glRotatef(45, 0, -1, 0);
        gl.glRotatef(angulo, 1, 0, 0);
        gl.glColor3f(0, 0.5f, 1);
        glut.glutSolidCone(1.5f, 3, 10, 8);
        gl.glPopMatrix();

        //Cilindro
        gl.glPushMatrix();
        gl.glTranslatef(12f, 3.0f, -20f);
        gl.glRotatef(45, 1, 1, 0);
        gl.glRotatef(angulo, -1, 1, 0);
        gl.glColor3f(0, 1, 1);
        glut.glutWireCylinder(1.5f, 3, 10, 8);
        gl.glPopMatrix();

        //Cilindro solido
        gl.glPushMatrix();
        gl.glTranslatef(12f, 11.0f, -20f);
        gl.glRotatef(45, 0, 1, 0);
        gl.glRotatef(angulo, 1, 0, 0);
        gl.glColor3f(1, 1, 0);
        glut.glutSolidCylinder(1.5f, 3, 10, 8);
        gl.glPopMatrix();

        //Cilindro contorno
        gl.glPushMatrix();
        gl.glColor3f(1, 1, 1);
        gl.glTranslatef(12f, 19.0f, -20f);
        gl.glRotatef(45, 0, 1, 0);
        gl.glRotatef(angulo, 0, 1, 0);
        glu.gluCylinder(quad, 1, 1, 4, 40, 40);
        gl.glPopMatrix();

        //Cubo
        gl.glPushMatrix();
        gl.glTranslatef(-10f, 1f, 0f);
        gl.glRotatef(45, -1, 0, 0);
        gl.glRotatef(angulo, 1, 0.5f, 0);
        gl.glColor3f(0.75f, 0.9f, 0.6f);
        glut.glutWireCube(2f);
        gl.glPopMatrix();

        //Cubo solido
        gl.glPushMatrix();
        gl.glTranslatef(-10f, 8f, 0f);
        gl.glRotatef(45, 1, 1, 0);
        gl.glRotatef(angulo, -1, 1, 0);
        gl.glColor3f(0.75f, 0.9f, 0);
        glut.glutSolidCube(2f);
        gl.glPopMatrix();

        //Toro/Dona
        gl.glPushMatrix();
        gl.glTranslatef(-18f, -13.5f, 0f);
        gl.glRotatef(45, 1, 0, 0);
        gl.glRotated(angulo, 0, 1, 0);
        gl.glColor3f(0.6f, 0.5f, 0.2f);
        glut.glutWireTorus(0.7f, 1.5, 10, 15);
        gl.glPopMatrix();

        //Toro/Dona solido
        gl.glPushMatrix();
        gl.glTranslatef(-18f, -6.5f, 0f);
        gl.glRotatef(45, -1, -1, 0);
        gl.glRotated(angulo, 1, -1, 0);
        gl.glColor3f(0.3f, 0.2f, 0.5f);
        glut.glutSolidTorus(0.7f, 1.5, 10, 15);
        gl.glPopMatrix();

        //Dodecaedro solido
        gl.glPushMatrix();
        gl.glTranslatef(-10f, -5.5f, 0f);
        gl.glRotatef(45, 1, 0, 0);
        gl.glRotatef(angulo, 1, 1, 0);
        gl.glColor3f(0.1f, 0.8f, 0.8f);
        glut.glutSolidDodecahedron();
        gl.glPopMatrix();

        //Dodecaedro
        gl.glPushMatrix();
        gl.glTranslatef(-10f, -13f, 0f);
        gl.glRotatef(75, 0, -1, 0);
        gl.glRotatef(angulo, 1, 0, 0);
        gl.glColor3f(0.91f, 0.8f, 0.32f);
        glut.glutWireDodecahedron();
        gl.glPopMatrix();

        //Isocaedro solido
        gl.glPushMatrix();
        gl.glTranslatef(1f, -6f, 0f);
        gl.glRotatef(45, 1, 0, 1);
        gl.glRotatef(angulo, 1, 1, 0);
        gl.glColor3f(0.15f, 0.18f, 0.85f);
        glut.glutSolidIcosahedron();
        gl.glPopMatrix();

        //Isocaedro
        gl.glPushMatrix();
        gl.glTranslatef(1f, -13.5f, 0f);
        gl.glRotatef(45, -1, 1, 0);
        gl.glRotatef(angulo, -1, 0.5f, 0);
        gl.glColor3f(0.1f, 0.0f, 0.9f);
        glut.glutWireIcosahedron();
        gl.glPopMatrix();

        //Tetera solido
        gl.glPushMatrix();
        gl.glTranslatef(12f, -6f, 0f);
        gl.glRotatef(45, 1, 1, 0);
        gl.glRotatef(angulo, 1, 1, 0);
        gl.glColor3f(0.9f, 0.1f, 0.8f);
        glut.glutSolidTeapot(1.5f);
        gl.glPopMatrix();

        //Terera
        gl.glPushMatrix();
        gl.glTranslatef(12f, -14f, 0f);
        gl.glRotatef(45, 0, 1, 0);
        gl.glRotatef(angulo, -1, 0, 0);
        gl.glColor3f(0.2f, 0.5f, 0.8f);
        glut.glutWireTeapot(1.5f);
        gl.glPopMatrix();

        //Octaedro solido
        gl.glPushMatrix();
        gl.glTranslatef(21f, -5.5f, 0f);
        gl.glRotatef(45, 1, 1, 0);
        gl.glRotatef(angulo, 1, 0, 0);
        gl.glColor3f(0.1f, 0.5f, 0.2f);
        glut.glutSolidOctahedron();
        gl.glPopMatrix();

        //Octaedro
        gl.glPushMatrix();
        gl.glTranslatef(21f, -14f, 0f);
        gl.glRotatef(45, 0, 1, 0);
        gl.glRotatef(angulo, -1, 0, 0);
        gl.glColor3f(0.1f, 0.6f, 0.4f);
        glut.glutWireOctahedron();
        gl.glPopMatrix();

        //Tetraedro solido
        gl.glPushMatrix();
        gl.glTranslatef(24f, 11.0f, -20f);
        gl.glRotatef(45, 1, 0, 0);
        gl.glRotatef(angulo, 1, -1, 0);
        gl.glColor3f(0.5f, 0.2f, 0.8f);
        glut.glutSolidTetrahedron();
        gl.glPopMatrix();

        //Tetraedro
        gl.glPushMatrix();
        gl.glTranslatef(24f, 4.0f, -20f);
        gl.glRotatef(45, 0, 1, 0);
        gl.glRotatef(angulo, -1, 1, 0);
        gl.glColor3f(0.5f, 1, 0.1f);
        glut.glutWireTetrahedron();
        gl.glPopMatrix();

        gl.glFlush();
        angulo++;
    }

    public void displayChanged(GLAutoDrawable drawable, boolean modeChanged, boolean deviceChanged) {
    }

    public void keyTyped(KeyEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void keyPressed(KeyEvent e) {
    }

    public void keyReleased(KeyEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
